#ifndef ZZGO_REPLAY_REPLAY_H
#define ZZGO_REPLAY_REPLAY_H

#include "engine.h"

struct engine *engine_replay_init(char *arg, struct board *b);

#endif
